<?php

include_once('../persistencia/conexion.php');

class clienteEmpresaDAO
{
    private $conex;
    public function __construct()
    {
        $this->conex = BaseDeDatos::conectar();

    }

    public function guardar($email, $cont, $tel, $calle, $numCalle, $barr, $rut, $rSoc){
        
        //Insert data into the client table
        //with Prepared Statement

        //Prepares a INSERT query statement
        $stmt = $this->conex->prepare("INSERT INTO cliente (email, contrasenia, dir_calle, dir_num, dir_barrio)
            VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param("sssis", $email, $cont, $calle, $numCalle, $barr);
        //Execute statement to query
        $stmt->execute();

        //Gets the assigned client number
        //prepares a SELECT query statement
        $stmt = $this->conex->prepare("SELECT nroCliente FROM cliente WHERE email = ?");
        $stmt->bind_param("s", $email);
        //Execute statement to query
        $stmt->execute();
        $result = $stmt->get_result();
        $nCliente = $result->fetch_assoc();
        //Saves the costumer number in numCli variable
        $numCli = ($nCliente['nroCliente']);

        //Inserts into cliente-telefono table
        $stmt = $this->conex->prepare("INSERT INTO telefono (nroCliente, num_cliente) VALUES (?, ?)");
        $stmt->bind_param("is", $numCli, $tel);
        $stmt->execute();

        //Inserts into cliente-empresa table 
        $stmt = $this->conex->prepare("INSERT INTO empresa (nroCliente, rut, razon_social) 
            VALUES (?, ?, ?)");
        $stmt->bind_param("iss", $numCli, $rut, $rSoc);
        $stmt->execute();

        //Close intance
        $stmt->close();
        //Close connection
        $this->conex->close();
    }

    
}