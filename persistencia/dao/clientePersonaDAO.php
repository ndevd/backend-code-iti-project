<?php

include_once('../persistencia/conexion.php');

class clientePersonaDAO
{
    private $conex;
    public function __construct()
    {
        $this->conex = BaseDeDatos::conectar();

    }

    public function guardar($email, $cont, $tel, $calle, $numCalle, $barr, $nom, $ape, $nDoc, $tDoc){
    //Insert data into the client table
    //with Prepared Statement

        //Prepares a INSERT query satement
        $stmt = $this->conex->prepare("INSERT INTO cliente (email, contrasenia, dir_calle, dir_num, dir_barrio)
                VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param("sssis", $email, $cont, $calle, $numCalle, $barr);
        //Execute statement to query
        $stmt->execute();

        //gets the assigned Client number
        //prepares a SELECT query statement
        $stmt = $this->conex->prepare("SELECT nroCliente FROM cliente WHERE email = ?");
        $stmt->bind_param("s", $email);
        //Execute statement to query
        $stmt->execute();
        $result = $stmt->get_result();
        $nCliente = $result->fetch_assoc();
        //Save the customer number in variable numCli
        $numCli = ($nCliente['nroCliente']);

        //Insert into cliente-telefono table
        $stmt = $this->conex->prepare("INSERT INTO telefono (nroCliente, num_cliente) VALUES (?, ?)");
        $stmt->bind_param("is", $numCli, $tel);
        $stmt->execute();

        //Insert into cliente-persona table
        $stmt = $this->conex->prepare("INSERT INTO persona (nroCliente, nombre, apellido, doc_tipo, doc_num) 
                VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param("issss", $numCli, $nom, $ape, $tDoc, $nDoc);
        $stmt->execute();

        //Close instance
        $stmt->close();
        //Close connection
        $this->conex->close();

    }
    

}