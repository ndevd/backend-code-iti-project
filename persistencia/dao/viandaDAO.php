<?php

include_once('../conexion.php');


class viandaDAO {

    private $conex;
    public function __construct()
    {
        $this->conex = BaseDeDatos::conectar();

    }

    public function listarTodas(){
        //Query all the data in the vianda table
        //with Prepared Statement
        $stmt = $this->conex ->prepare("SELECT * FROM vianda ORDER BY nombre");
        $stmt->execute();
        $result = $stmt->get_result();
        
        //This creates array of food items 
        //and fills it with all the elements that were obtained from the query
        $viandas = [];
        while($row = $result->fetch_assoc()){
            $viandas[] = $row;
        }

        //Close intstance
        $stmt->close();
        //Close connection
        $this->conex ->close();

        //var_dump($viandas);
        return $viandas;
    }

    
}