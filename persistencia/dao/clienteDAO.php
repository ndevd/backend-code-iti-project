<?php

include_once('../conexion.php');

class clienteDAO
{
    private $conex;
    public function __construct()
    {
        $this->conex = BaseDeDatos::conectar();

    }

    public function login($u, $c){
    //Login function
    //search the user in the database and checks if the password entered is correct
        //prepares a SELECT query statement
        $stmt = $this->conex->prepare("SELECT * FROM cliente WHERE email = ?");
        $stmt->bind_param("s", $u);
        //Execute statement to query
        $stmt->execute();
        //Save the result
        $result = $stmt->get_result();
        $usuario = $result->fetch_object();
        //Verify the password
        $verify = password_verify($c, $usuario->contrasenia);
        //returns variable verify as true or false
        return $verify;
    }
    

}