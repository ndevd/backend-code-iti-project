<?php

require 'funciones.php';
require 'clientePersona.php';
require 'clienteEmpresa.php';
include '../persistencia/dao/clienteDAO.php';
include '../persistencia/dao/clientePersonaDAO.php';
include '../persistencia/dao/clienteEmpresaDAO.php';

    
    switch ($_REQUEST['accion']) {

        case ('insert-cliente-persona'):
            //////////////////////////////insert - cliente persona/////////////////////////////////////
            $email = $_POST['email'];
            $cont = $_POST['contrasenia'];
            $tel = $_POST['telefono'];
            $calle = $_POST['calle'];
            $nCalle = $_POST['numerocalle'];
            $barr = $_POST['barrio'];

            $nom = $_POST['nombre'];
            $ape = $_POST['apellido'];
            $nroDoc = $_POST['nrodocumento'];
            $tipoDoc = $_POST['tipodocumento'];

            $camposLlenos;
            //Check that each field is not empty
            foreach ($_POST as $campos) {
                if (!empty(trim($campos))) {
                    $camposLlenos = true;
                } else {
                    $camposLlenos = false;
                }
            }

            //Saves the result obtained when invoking the validaPersona function (true or false) in the datosValidos variable.
            $datosValidos = validaPersona($email, $cont, $calle, $nCalle, $tel, $barr, $nom, $ape, $nroDoc, $tipoDoc);

            //VALIDATION
            //Check that all fields are filled out and if the data entered is valid
            if ($camposLlenos && $datosValidos) {
        
                //creates person object
                $persona = new Persona();
                //set attributes to the object
                $persona->setEmail($email);
                $persona->setContrasenia($cont);
                $persona->setTelefono($tel);
                $persona->setCalle($calle);
                $persona->setNumCalle($nCalle);
                $persona->setBarrio($barr);
                $persona->setNombre($nom);
                $persona->setApellido($ape);
                $persona->setNroDocumento($nroDoc);
                $persona->setTipoDocumento($tipoDoc);

                //creates clientePersonaDAO object
                $insertPersona = new clientePersonaDAO();
                //Invokes the save function to make inserts to the database
                $insertPersona->guardar(
                    $persona->getEmail(), $persona->getContrasenia(), $persona->getTelefono(),
                    $persona->getCalle(), $persona->getNumCalle(), $persona->getBarrio(), $persona->getNombre(),
                    $persona->getApellido(), $persona->getNroDocumento(), $persona->getTipoDocumento()
                );
                header('Location: ../presentacion/index.html');
            } else {
                //echo "error";
            }

            break;

        case ('insert-cliente-empresa'):
            //////////////////////////////insert - cliente empresa/////////////////////////////////////
            $email = $_POST['email'];
            $cont = $_POST['contrasenia'];
            $tel = $_POST['telefono'];
            $calle = $_POST['calle'];
            $nCalle = $_POST['numerocalle'];
            $barr = $_POST['barrio'];
            
            $rut = $_POST['rut'];
            $rSocial = $_POST['razon-social'];

            $camposLlenos;

            foreach ($_POST as $campos) {
                if (!empty(trim($campos))) {
                    $camposLlenos = true;
                } else {
                    $camposLlenos = false;
                }
            }


            $datosValidos = validaEmpresa($email, $cont, $calle, $nCalle, $tel, $barr, $rut, $rSocial);

            //Check that all fields are filled
            if ($camposLlenos && $datosValidos) {

                $empresa = new Empresa();

                $empresa->setEmail($email);
                $empresa->setContrasenia($cont);
                $empresa->setTelefono($tel);
                $empresa->setCalle($calle);
                $empresa->setNumCalle($nCalle);
                $empresa->setBarrio($barr);
                $empresa->setRut($rut);
                $empresa->setRazonSocial($rSocial);

                $insertEmpresa = new clienteEmpresaDAO();
                $insertEmpresa->guardar(
                    $empresa->getEmail(), $empresa->getContrasenia(), $empresa->getTelefono(), $empresa->getCalle(),
                    $empresa->getNumCalle(), $empresa->getBarrio(), $empresa->getRut(), $empresa->getRazonSocial()
                );
                header('Location: ../presentacion/index.html');
            } else {
                //echo "error";
            }
            break;

        case ('login'):
            //////////////////////////////Login/////////////////////////////////////
            $usu = $_POST['usuario'];
            $con = $_POST['contrasenia'];
            $clienteLogin = new clienteDAO();
            $login = $clienteLogin->login($usu, $con);

            session_start();
            if ($login) { //if $login is true
                $_SESSION['usuario'] = $usu;  
            }else{ //if $login is false
                $_SESSION['usuario'] = 'usuario incorrecto';
            }
            sleep(2);
            header('Location: ../presentacion/index.html');
            break;

        case ('logout'):
        ////////////////////////////////Logout///////////////////////////////////////    
            session_start();
            session_destroy();
            sleep(2);
            header('Location: ../presentacion/index.html');
            break;
    }

