<?php

include_once('cliente.php');


class Persona extends Cliente
{
    //Properties
    protected $nombre;
    protected $apellido;
    protected $nroDocumento;
    protected $tipoDocumento;

    //Constructor
    public function __construct()
    {
        parent::__construct();

    }


    //Cliente-Persona Setters
    public function setNombre($nom)
    {
        $this->nombre = $nom;
    }
    public function setApellido($ape)
    {
        $this->apellido = $ape;
    }
    public function setNroDocumento($ndoc)
    {
        $this->nroDocumento = $ndoc;
    }
    public function setTipoDocumento($tipodoc)
    {
        $this->tipoDocumento = $tipodoc;
    }
    //Cliente-Persona Getters
    public function getNombre()
    {
        return $this->nombre;
    }
    public function getApellido()
    {
        return $this->apellido;
    }
    public function getNroDocumento()
    {
        return $this->nroDocumento;
    }
    public function getTipoDocumento()
    {
        return $this->tipoDocumento;
    }


}