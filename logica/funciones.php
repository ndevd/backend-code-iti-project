<?php

function validaPersona($email, $cont, $calle, $nCalle, $tel, $barr, $nom, $ape, $nroDoc, $tipoDoc){
//Checks if the data entered is valid   
    if( //email 
        ( is_string($email) && filter_var($email, FILTER_VALIDATE_EMAIL) ) &&
        //password
        ( is_string($cont)  && strlen($cont)>6 ) &&
        //street
        ( is_string($calle) && preg_match("/[a-zA-Z ]+/", $calle) ) &&
        //street number
        ( is_numeric($nCalle) && filter_var($nCalle, FILTER_VALIDATE_INT) ) &&
        //phone number
        ( is_numeric($tel)  && preg_match("/[0-9]/", $tel) ) &&
        //neighborhood
        ( is_string($barr)  && preg_match("/[a-zA-Z ]+/", $barr) ) &&
        //name
        ( is_string($nom)   && preg_match("/[a-zA-Z ]+/", $nom) ) &&
        //last name
        ( is_string($ape)   && preg_match("/[a-zA-Z ]+/", $ape) ) &&
        //document number
        ( is_numeric($nroDoc)  && preg_match("/[0-9]/", $nroDoc) ) &&
        //document type
        ( is_string($tipoDoc)  && preg_match("/[a-zA-Z ]+/", $tipoDoc) )
    ){
        $valido = true;
    }else{
        //error
        $valido = false;  
    }

    return $valido;
}

function validaEmpresa($email, $cont, $calle, $nCalle, $tel, $barr, $rut, $rSocial){
//Checks if the data entered is valid   
    if( //email
        ( is_string($email) && filter_var($email, FILTER_VALIDATE_EMAIL) ) &&
        //password
        ( is_string($cont)  && strlen($cont)>6 ) &&
        //street
        ( is_string($calle) && preg_match("/[a-zA-Z ]+/", $calle) ) &&
        //street number
        ( is_numeric($nCalle) && filter_var($nCalle, FILTER_VALIDATE_INT) ) &&
        //phone number
        ( is_numeric($tel)  && preg_match("/[0-9]/", $tel) ) &&
        //neighborhood
        ( is_string($barr)  && preg_match("/[a-zA-Z ]+/", $barr) ) &&
        //rut
        ( is_numeric($rut)  && preg_match("/[0-9]/", $rut) ) &&
        //rbusiness name
        ( is_string($rSocial)  && preg_match("/[a-zA-Z ]+/", $rSocial) )
    ){
        $valido = true;
    }else{
        //error
        $valido = false;
    }

    return $valido;
}